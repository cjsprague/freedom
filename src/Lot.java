
public class Lot {
	public ICar[] cars;
	public final int MAX_CARS = 5;
	public Lot()
	{
		cars = new ICar[MAX_CARS];
		cars[0] = new HondaAccord(2016, 1);
		cars[1] = new SubaruOutback(2014, 300000);
		
		for(ICar car : cars)
		{
			if (car!=null)
				System.out.println("On this Lot we have a " + car);
		}
	}

}
